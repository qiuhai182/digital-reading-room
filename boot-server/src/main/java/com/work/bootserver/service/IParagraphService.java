package com.work.bootserver.service;

import com.work.bootserver.entity.Paragraph;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
public interface IParagraphService extends IService<Paragraph> {

    public List<Paragraph> findParagraphByChapterId(int chapterId);

    public List<Paragraph> findparagraphByChapterIdAndNumbering(int chapterId, Integer numbering);
}

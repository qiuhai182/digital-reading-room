package com.work.bootserver.service;

import com.work.bootserver.entity.Book;
import com.work.bootserver.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author suLang
 * @since 2022-06-07
 */
public interface IUserService extends IService<User> {
    public User findUserByEmail(String email);
    public User findUserByName(String userName);
    public List<Book> findMyFavorite (Long id);
    public List<User> findAllUser();
    public int addUser(User registerUser);
    public int update(User user);
    public User findUserById(Long id);
}

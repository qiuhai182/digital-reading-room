package com.work.bootserver.service.impl;

import com.work.bootserver.entity.Menu;
import com.work.bootserver.mapper.MenuMapper;
import com.work.bootserver.service.IMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author suLang
 * @since 2022-06-07
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    @Autowired
    private MenuMapper menuMapper;
    @Override
    public List<String> findMenuById(Long userid) {
        return menuMapper.selectPermsByUserId(userid);
    }
}

package com.work.bootserver.service;

import com.work.bootserver.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author suLang
 * @since 2022-06-07
 */
public interface IRoleService extends IService<Role> {

}

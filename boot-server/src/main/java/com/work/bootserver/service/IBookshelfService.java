package com.work.bootserver.service;

import com.work.bootserver.entity.Bookshelf;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author suLang
 * @since 2022-06-06
 */
public interface IBookshelfService extends IService<Bookshelf> {

    public int update(Bookshelf bookshelf);

    public Bookshelf getBookMark(Long userId, int bookId);

    public int addBookshelf(Bookshelf bookshelf);
}

package com.work.bootserver.service;

import com.work.bootserver.entity.User_role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author suLang
 * @since 2022-06-07
 */
public interface IUser_roleService extends IService<User_role> {
    public int addUserRole(Long userId, Long roleId);
}

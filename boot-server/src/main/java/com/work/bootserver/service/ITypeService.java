package com.work.bootserver.service;

import com.work.bootserver.entity.Type;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author suLang
 * @since 2022-06-06
 */
public interface ITypeService extends IService<Type> {

}

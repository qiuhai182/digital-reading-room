package com.work.bootserver.service;

import com.work.bootserver.entity.ResponseResult;
import com.work.bootserver.entity.User;
import org.springframework.stereotype.Service;

public interface LoginService {
    public ResponseResult login(User user);
    public ResponseResult logout();
    public User getLoginUser();
}

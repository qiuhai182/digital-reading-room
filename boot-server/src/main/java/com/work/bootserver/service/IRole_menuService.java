package com.work.bootserver.service;

import com.work.bootserver.entity.Role_menu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author suLang
 * @since 2022-06-07
 */
public interface IRole_menuService extends IService<Role_menu> {

}

package com.work.bootserver.service.impl;

import com.work.bootserver.entity.Book_type;
import com.work.bootserver.mapper.Book_typeMapper;
import com.work.bootserver.service.IBook_typeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author suLang
 * @since 2022-06-06
 */
@Service
public class Book_typeServiceImpl extends ServiceImpl<Book_typeMapper, Book_type> implements IBook_typeService {

}

package com.work.bootserver.service.impl;

import com.work.bootserver.entity.Chapter;
import com.work.bootserver.mapper.ChapterMapper;
import com.work.bootserver.service.IChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
@Service
public class ChapterServiceImpl extends ServiceImpl<ChapterMapper, Chapter> implements IChapterService {

    @Autowired
    private ChapterMapper chapterMapper;
    @Override
    public Chapter getChapterByBookIdAndNumbering(int bookId, int numbering) {
        return chapterMapper.selectByBookIdAndNumbering(bookId,numbering);
    }

    @Override
    public List<Chapter> getChapterByBookId(int bookId) {
        return chapterMapper.selectByBookId(bookId);
    }
}

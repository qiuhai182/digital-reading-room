package com.work.bootserver.service;

import com.work.bootserver.entity.Reply;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
public interface IReplyService extends IService<Reply> {

    public int addReply(Reply reply);

    public List<Reply> findReplyByCommentId(int commentId);
}

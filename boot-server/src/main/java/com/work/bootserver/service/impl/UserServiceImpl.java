package com.work.bootserver.service.impl;

import com.work.bootserver.entity.Book;
import com.work.bootserver.entity.User;
import com.work.bootserver.mapper.UserMapper;
import com.work.bootserver.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author suLang
 * @since 2022-06-07
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private UserMapper userMapper;
    @Override
    public User findUserByEmail(String email) {
        return userMapper.selectByemail(email);
    }

    @Override
    public User findUserByName(String userName) {
        return userMapper.selectByName(userName);
    }

    @Override
    public List<Book> findMyFavorite(Long id) {
        return userMapper.selectMyFavorite(id);
    }

    @Override
    public List<User> findAllUser() {
        return userMapper.selectList(null);
    }

    @Override
    public int addUser(User registerUser) {
        return userMapper.insert(registerUser);
    }

    @Override
    public int update(User user) {
        return userMapper.updateById(user);
    }

    @Override
    public User findUserById(Long id) {
        return userMapper.selectById(id);
    }
}

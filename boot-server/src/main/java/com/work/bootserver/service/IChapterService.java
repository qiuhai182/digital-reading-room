package com.work.bootserver.service;

import com.work.bootserver.entity.Chapter;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
public interface IChapterService extends IService<Chapter> {

    public Chapter getChapterByBookIdAndNumbering(int bookId, int numbering);

    public List<Chapter> getChapterByBookId(int bookId);
}

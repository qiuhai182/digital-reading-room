package com.work.bootserver.service.impl;

import com.work.bootserver.entity.Bookshelf;
import com.work.bootserver.mapper.BookshelfMapper;
import com.work.bootserver.service.IBookshelfService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author suLang
 * @since 2022-06-06
 */
@Service
public class BookshelfServiceImpl extends ServiceImpl<BookshelfMapper, Bookshelf> implements IBookshelfService {

    @Autowired
    private BookshelfMapper bookshelfMapper;
    @Override
    public int update(Bookshelf bookshelf) {
        return bookshelfMapper.updateById(bookshelf);
    }

    @Override
    public Bookshelf getBookMark(Long userId, int bookId) {
        return bookshelfMapper.selectByUserIdAndBookId(userId,bookId);
    }

    @Override
    public int addBookshelf(Bookshelf bookshelf) {
        return bookshelfMapper.insert(bookshelf);
    }
}

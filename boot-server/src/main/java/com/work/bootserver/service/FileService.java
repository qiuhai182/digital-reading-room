package com.work.bootserver.service;

import com.work.bootserver.entity.ResponseResult;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface FileService {
    public String upload(MultipartFile file) throws IOException;
    public void getFiles(String flag, HttpServletResponse response);
}

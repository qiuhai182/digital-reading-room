package com.work.bootserver.service.impl;

import com.work.bootserver.entity.Role;
import com.work.bootserver.mapper.RoleMapper;
import com.work.bootserver.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author suLang
 * @since 2022-06-07
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}

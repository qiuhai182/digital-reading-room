package com.work.bootserver.service.impl;

import com.work.bootserver.entity.User_role;
import com.work.bootserver.mapper.User_roleMapper;
import com.work.bootserver.service.IUser_roleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author suLang
 * @since 2022-06-07
 */
@Service
public class User_roleServiceImpl extends ServiceImpl<User_roleMapper, User_role> implements IUser_roleService {

    @Autowired
    User_roleMapper user_roleMapper;
    @Override
    public int addUserRole(Long userId, Long roleId) {
        User_role user_role = new User_role(userId,roleId);
        return user_roleMapper.insert(user_role);
    }
}

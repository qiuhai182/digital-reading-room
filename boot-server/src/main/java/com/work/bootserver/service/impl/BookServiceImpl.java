package com.work.bootserver.service.impl;

import com.work.bootserver.entity.Book;
import com.work.bootserver.entity.Chapter;
import com.work.bootserver.mapper.BookMapper;
import com.work.bootserver.mapper.ChapterMapper;
import com.work.bootserver.service.IBookService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author suLang
 * @since 2022-06-06
 */
@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements IBookService {

    @Autowired
    private BookMapper bookMapper;
    @Autowired
    private ChapterMapper chapterMapper;
    @Override
    public Book findBookById(Integer id) {
        return bookMapper.selectById(id);
    }

    @Override
    public List<Book> findBookByAuthor(String author) {
        return bookMapper.selectByAuthor(author);
    }

    @Override
    public Book findBookByName(String bookName) {
        return bookMapper.selectByName(bookName);
    }

    @Override
    public int addBook(Book book) {
        return bookMapper.insert(book);
    }

    @Override
    public int update(Book book) {
        return bookMapper.updateById(book);
    }

    @Override
    public List<Book> findBookByType(String bookType) {
        return bookMapper.selectByType(bookType);
    }

    @Override
    public int getBookmark(Long userId, int bookId) {
        return bookMapper.getBookmark(userId,bookId);
    }

    @Override
    public List<Book> findAllBook() {
        return bookMapper.selectList(null);
    }

    @Override
    public int addChapter(Chapter chapter) {
        chapter.setNumbering(chapterMapper.selectList(null).size()+1);
        return chapterMapper.insert(chapter);
    }

    @Override
    public Chapter getChapterById(int chapterId) {
        return chapterMapper.selectById(chapterId);
    }
}

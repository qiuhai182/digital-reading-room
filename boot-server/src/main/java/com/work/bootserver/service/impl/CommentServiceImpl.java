package com.work.bootserver.service.impl;

import com.work.bootserver.entity.Comment;
import com.work.bootserver.entity.Paragraph;
import com.work.bootserver.mapper.CommentMapper;
import com.work.bootserver.mapper.ParagraphMapper;
import com.work.bootserver.service.ICommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements ICommentService {

    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private ParagraphMapper paragraphMapper;
    @Override
    public int addComment(Comment comment) {
        return commentMapper.insert(comment);
    }

    @Override
    public int deleteComment(int commentId) {
        return commentMapper.deleteById(commentId);
    }

    @Override
    public int getLastId() {
        return commentMapper.getLastId();
    }

    @Override
    public int addParagraph(Paragraph paragraph) {
        return paragraphMapper.insert(paragraph);
    }

    @Override
    public List<Comment> getParagraphComment(int numbering, int chapterId, int bookId) {
        return commentMapper.selectParagraphComment(chapterId,numbering,bookId);
    }
}

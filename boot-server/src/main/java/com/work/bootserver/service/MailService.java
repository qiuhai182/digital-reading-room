package com.work.bootserver.service;

import cn.hutool.core.util.RandomUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class MailService {
    @Value("${spring.mail.username}")
    private String adminEmail;

    private String nicname = "电子阅览室网站系统";

    @Resource
    private JavaMailSender mailSender;


//    @Resource
//    private TemplateEngine templateEngine;

    public String sendMailForActivationAccount(String mailBox){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(nicname+'<'+adminEmail+'>');
        message.setSentDate(new Date());
        message.setSubject("欢迎注册电子阅览室账号");
        String code = RandomUtil.randomNumbers(4);
        message.setText("本次注册的验证码为"+code);
        message.setTo(mailBox);
        mailSender.send(message);
        return code;
//        try {
//            message.setFrom(nicname+'<'+adminEmail+'>');
//            message.setSentDate(new Date());
//            message.setSubject("欢迎注册电子阅览室账号");
//            String code = RandomUtil.randomNumbers(4);
//            message.setText("本次注册的验证码为"+code);
//            message.setTo(mailBox);
//            mailSender.send(message);
//            return code;
//        }catch (Exception e){
//            return "failed";
//        }
    }
}

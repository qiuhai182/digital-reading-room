package com.work.bootserver.service.impl;

import com.work.bootserver.entity.Reply;
import com.work.bootserver.mapper.ReplyMapper;
import com.work.bootserver.service.IReplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
@Service
public class ReplyServiceImpl extends ServiceImpl<ReplyMapper, Reply> implements IReplyService {

    @Autowired
    private ReplyMapper replyMapper;
    @Override
    public int addReply(Reply reply) {
        return replyMapper.insert(reply);
    }

    @Override
    public List<Reply> findReplyByCommentId(int commentId) {
        return replyMapper.selectByCommentId(commentId);
    }
}

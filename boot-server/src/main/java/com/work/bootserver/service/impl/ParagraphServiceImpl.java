package com.work.bootserver.service.impl;

import com.work.bootserver.entity.Paragraph;
import com.work.bootserver.mapper.ParagraphMapper;
import com.work.bootserver.service.IParagraphService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
@Service
public class ParagraphServiceImpl extends ServiceImpl<ParagraphMapper, Paragraph> implements IParagraphService {

    @Autowired
    public ParagraphMapper paragraphMapper;

    @Override
    public List<Paragraph> findParagraphByChapterId(int chapterId) {
        return paragraphMapper.selectByChapterId(chapterId);
    }

    @Override
    public List<Paragraph> findparagraphByChapterIdAndNumbering(int chapterId, Integer numbering) {
        return paragraphMapper.selectByChapterIdAndNumbering(chapterId,numbering);
    }
}

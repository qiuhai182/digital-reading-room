package com.work.bootserver.service.impl;

import com.work.bootserver.entity.Type;
import com.work.bootserver.mapper.TypeMapper;
import com.work.bootserver.service.ITypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author suLang
 * @since 2022-06-06
 */
@Service
public class TypeServiceImpl extends ServiceImpl<TypeMapper, Type> implements ITypeService {

}

package com.work.bootserver.service;

import com.work.bootserver.entity.Book;
import com.baomidou.mybatisplus.extension.service.IService;
import com.work.bootserver.entity.Chapter;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author suLang
 * @since 2022-06-06
 */
public interface IBookService extends IService<Book> {
    public Book findBookById(Integer id);
    public List<Book> findBookByAuthor(String author);
    public Book findBookByName(String bookName);
    public int addBook(Book book);
    public int update(Book book);
    public List<Book> findBookByType(String bookType);

    public int getBookmark(Long userId, int bookId);

    public List<Book> findAllBook();

    public int addChapter(Chapter chapter);

    public Chapter getChapterById(int chapterId);
}

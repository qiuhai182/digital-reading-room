package com.work.bootserver.service;

import com.work.bootserver.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author suLang
 * @since 2022-06-07
 */
public interface IMenuService extends IService<Menu> {
    public List<String> findMenuById(Long userid);
}

package com.work.bootserver.service;

import com.work.bootserver.entity.Comment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.work.bootserver.entity.Paragraph;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
public interface ICommentService extends IService<Comment> {

    public int addComment(Comment comment);

    public int deleteComment(int commentId);

    public int getLastId();

    public int addParagraph(Paragraph paragraph);

    public List<Comment> getParagraphComment(int chapterId, int numbering, int bookId);
}

package com.work.bootserver.service.impl;

import com.work.bootserver.entity.Role_menu;
import com.work.bootserver.mapper.Role_menuMapper;
import com.work.bootserver.service.IRole_menuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author suLang
 * @since 2022-06-07
 */
@Service
public class Role_menuServiceImpl extends ServiceImpl<Role_menuMapper, Role_menu> implements IRole_menuService {

}

package com.work.bootserver.controller;

import com.alibaba.fastjson.JSONObject;
import com.work.bootserver.entity.Code;
import com.work.bootserver.entity.ResponseResult;
import com.work.bootserver.repository.CodeRepository;
import com.work.bootserver.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/code")
public class CodeController {
    @Autowired
    private MailService mailService;
    @Autowired
    private CodeRepository codeRepository;
    @RequestMapping("/sendCode")
    public ResponseResult sendCode(@RequestBody String json){
        //发送邮件
        JSONObject jsonObject = JSONObject.parseObject(json);
        String email = jsonObject.getString("email");
        String codeType = jsonObject.getString("codeType");
        String codenumber = mailService.sendMailForActivationAccount(email);
        if (!StringUtils.hasText(codenumber)){
            return new ResponseResult(250,"发送失败");
        }
        Code code = new Code(codenumber,codeType);
        codeRepository.saveCode(email,code);
        Map<String,String> map = new HashMap<>();
        return new ResponseResult(200,"发送成功");
    }
}

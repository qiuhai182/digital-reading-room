package com.work.bootserver.controller;


import com.alibaba.fastjson.JSONObject;
import com.work.bootserver.entity.*;
import com.work.bootserver.repository.CodeRepository;
import com.work.bootserver.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author suLang
 * @since 2022-06-07
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;
    @Autowired
    private LoginService loginService;
    @Autowired
    private IUser_roleService user_roleService;
    @Autowired
    private FileService fileService;
    @Autowired
    private CodeRepository codeRepository;
    @Autowired
    private ICommentService commentService;

    @RequestMapping("/register")
    public ResponseResult register(@RequestBody String json){

        JSONObject jsonObject = JSONObject.parseObject(json);
        String code = jsonObject.getString("code");
        String userName = jsonObject.getString("userName");
        String email = jsonObject.getString("email");
        String password = jsonObject.getString("password");
        String avatar = jsonObject.getString("avatar");

        Object theCode = codeRepository.getCode(email);
        User registerUser = new User();
        registerUser.setEmail(email);
        registerUser.setUserName(userName);
        registerUser.setAvatar(avatar);
        //密码加密存储
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        password = passwordEncoder.encode(password);
        registerUser.setPassword(password);
        //设置创建时间
        registerUser.setCreateTime(LocalDateTime.now());
        if (theCode instanceof Code && ((Code)theCode).getCodeNumber().equals(code) && ((Code)theCode).getCodeType().equals("register")){
            if (userService.addUser(registerUser)==1){
                //默认用户的角色id为2（即普通用户)
                Long roleId = 2L;
                int i = user_roleService.addUserRole((long) commentService.getLastId(),roleId);
                if (i==1){
                    return new ResponseResult(200,"注册成功");
                }else {
                    return new ResponseResult(250,"注册失败");
                }
            }else {
                return new ResponseResult(250,"注册失败");
            }
        }else {
            return new ResponseResult(250,"验证码错误");
        }
    }
    @RequestMapping("/changePassword")
    public ResponseResult changePassword(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        String code = jsonObject.getString("code");
        String password = jsonObject.getString("password");
        User user = loginService.getLoginUser();
        Object theCode = codeRepository.getCode(user.getEmail());
        if (theCode instanceof Code && ((Code)theCode).getCodeNumber().equals(code) && ((Code)theCode).getCodeType().equals("changePassword")){
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            password = passwordEncoder.encode(password);
            user.setPassword(password);
            int i = userService.update(user);
            if (i==1){
                return new ResponseResult(200,"修改成功");
            }else {
                return new ResponseResult(250,"修改失败");
            }
        }else {
            return new ResponseResult(250,"验证码错误");
        }
    }

    @RequestMapping("/login")
    public ResponseResult login(@RequestBody String json,HttpServletResponse response){
        JSONObject jsonObject = JSONObject.parseObject(json);
        String name = jsonObject.getString("userName");
        String password =jsonObject.getString("password");
        User user = new User();
        user.setUserName(name);
        user.setPassword(password);
        ResponseResult result = loginService.login(user);
        Map map = (Map) result.getData();
        String token = (String) map.get("token");
        response.setHeader("token",token);
        System.out.println();
        return result;
    }

    @PreAuthorize("hasAuthority('个人信息管理')")
    @RequestMapping("/logout")
    public ResponseResult logout(){
        return loginService.logout();
    }

    @RequestMapping("/hello")
    public String hello(){
        return "hello";
    }


    @PreAuthorize("hasAuthority('个人信息管理')")
    @RequestMapping("/findSelf")
    public ResponseResult findSelf(){
        User user = loginService.getLoginUser();
        Map<String,User> map = new HashMap<>();
        map.put("user",user);
        return new ResponseResult(200,"查找成功",map);
    }

    @PreAuthorize("hasAuthority('个人信息管理')")
    @RequestMapping("/myFavorite")
    public ResponseResult myFavorit(){
        User user = loginService.getLoginUser();
        Long userid = user.getId();
        List<Book> books = userService.findMyFavorite(userid);
        Map<String,List<Book>> map = new HashMap<>();
        map.put("myFavorite",books);
        return new ResponseResult(200,"查找成功",map);
    }


    @PreAuthorize("hasAuthority('个人信息管理')")
    @RequestMapping("/updateSelf")
    public ResponseResult updateSelf(@RequestBody User user){
        if (userService.update(user)==1){
            return new ResponseResult(200,"修改成功");
        }else {
            return new ResponseResult(250,"修改失败");
        }
    }

    //上传头像
    @PreAuthorize("hasAuthority('个人信息管理')")
    @RequestMapping("/setAvatar")
    public ResponseResult setAvatar(MultipartFile file) throws IOException {
        User user = loginService.getLoginUser();
        String flag = fileService.upload(file);
        user.setAvatar(flag);
        userService.update(user);
        Map<String,String> map = new HashMap<>();
        map.put("avatarUrl",flag);
        return new ResponseResult(200,"上传成功",map);
    }

    //下载文件  (下载文件移交至FileController统一处理
//    @PreAuthorize("hasAuthority('个人信息管理')")
//    @RequestMapping("/{flag}")
//    public ResponseResult getFiels(@PathVariable String flag, HttpServletResponse response){
//        fileService.getFiles(flag,response);
//        return new ResponseResult(200,"下载成功");
//    }

    @PreAuthorize("hasAuthority('用户管理')")
    @RequestMapping("/findAllUser")
    public ResponseResult findAllUser(){
        List<User> users = userService.findAllUser();
        Map<String,List<User>> map = new HashMap<>();
        map.put("users",users);
        return new ResponseResult(200,"查找成功",map);
    }

    @RequestMapping("findUserById")
    public ResponseResult findUserById(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        Long id = Long.valueOf(jsonObject.getInteger("userId"));
        User user = userService.findUserById(id);
        Map<String,User> map = new HashMap<>();
        map.put("user",user);
        if (user == null){
            return new ResponseResult(250,"查找失败");
        }
        return new ResponseResult(200,"查找成功",map);
    }

//    @RequestMapping("/test")
//    public String test(@RequestBody String s){
//        JSONObject jsonObject = JSONObject.parseObject(s);
//        String name = jsonObject.getString("name");
//        return name;
//    }
}


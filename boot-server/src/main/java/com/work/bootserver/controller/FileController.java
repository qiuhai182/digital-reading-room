package com.work.bootserver.controller;

import com.work.bootserver.entity.ResponseResult;
import com.work.bootserver.entity.User;
import com.work.bootserver.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
@RestController
@RequestMapping("/files")
public class FileController {
    @Autowired
    FileService fileService;

    @PreAuthorize("hasAnyAuthority('个人信息管理')")
    @RequestMapping("/upload")
    public ResponseResult upload(MultipartFile file) throws IOException {
        String flag = fileService.upload(file);
        Map<String,String> map = new HashMap<>();
        map.put("flag",flag);
        return new ResponseResult(200,"上传成功",map);
    }

    @RequestMapping("/{flag}")
    public void getFiels(@PathVariable String flag, HttpServletResponse response){
        fileService.getFiles(flag,response);
    }
}

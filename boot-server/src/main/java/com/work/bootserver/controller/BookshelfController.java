package com.work.bootserver.controller;


import com.alibaba.fastjson.JSONObject;
import com.work.bootserver.entity.Bookshelf;
import com.work.bootserver.entity.ResponseResult;
import com.work.bootserver.service.IBookService;
import com.work.bootserver.service.IBookshelfService;
import com.work.bootserver.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author suLang
 * @since 2022-06-06
 */
@RestController
@RequestMapping("/bookshelf")
public class BookshelfController {
    @Autowired
    private LoginService loginService;
    @Autowired
    private IBookService bookService;
    @Autowired
    private IBookshelfService bookshelfService;

    @PreAuthorize("hasAuthority('书籍查看')")
    @RequestMapping("collect")
    public ResponseResult addCollect(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        int bookId = jsonObject.getInteger("bookId");
        Long userId = loginService.getLoginUser().getId();
        Bookshelf bookshelf = new Bookshelf();
        bookshelf.setBookId(bookId);
        bookshelf.setUserId(userId);
        bookshelf.setBookmark(0);
        if (bookshelfService.addBookshelf(bookshelf)==1){
            return new ResponseResult(200,"收藏成功");
        }else {
            return new ResponseResult(250,"收藏失败");
        }
    }
    @PreAuthorize("hasAuthority('书籍查看')")
    @RequestMapping("/getBookmark")
    public ResponseResult getBookmark(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        int bookId = jsonObject.getInteger("bookId");
        Long userId = loginService.getLoginUser().getId();
        Map<String,Integer> map = new HashMap<>();
        int bookmark = bookService.getBookmark(userId,bookId);
        map.put("bookmark",bookmark);
        return new ResponseResult(200,"查找成功",map);
    }

    @PreAuthorize("hasAuthority('书籍查看')")
    @RequestMapping("/setBookmark")
    public ResponseResult setBookMark(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        int bookId = jsonObject.getInteger("bookId");
        int bookmark = jsonObject.getInteger("bookmark");
        Long userId = loginService.getLoginUser().getId();
        Bookshelf bookshelf = new Bookshelf();
        bookshelf.setId(bookshelfService.getBookMark(userId,bookId).getId());
        bookshelf.setBookId(bookId);
        bookshelf.setUserId(userId);
        bookshelf.setBookmark(bookmark);
        int i = bookshelfService.update(bookshelf);
        if (i==1){
            return new ResponseResult(200,"修改成功");
        }else {
            return new ResponseResult(250,"修改失败");
        }
    }
}


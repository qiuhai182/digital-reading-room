package com.work.bootserver.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
@RestController
@RequestMapping("/paragraph")
public class ParagraphController {

}


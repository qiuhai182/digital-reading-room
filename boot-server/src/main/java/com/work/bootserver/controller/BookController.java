package com.work.bootserver.controller;


import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSONObject;
import com.work.bootserver.entity.Book;
import com.work.bootserver.entity.Chapter;
import com.work.bootserver.entity.ResponseResult;
import com.work.bootserver.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author suLang
 * @since 2022-06-06
 */
@RestController
@RequestMapping("/book")
public class BookController {
    @Autowired
    private IBookService bookService;
    @Autowired
    private FileService fileService;
    @Autowired
    private LoginService loginService;
    @Autowired
    private ICommentService commentService;
    @Autowired
    private IChapterService chapterService;

    @Value("${server.port}")
    private String port;

    @Value("${file.ip}")
    private String ip;

    @PreAuthorize("hasAuthority('书籍查看')")
    @RequestMapping("/findAllBook")
    public ResponseResult findAllBook(){
        Map<String,List<Book>> map = new HashMap<>();
        List<Book> books = bookService.findAllBook();
        map.put("books",books);
        return new ResponseResult(200,"查找成功",map);
    }


    @PreAuthorize("hasAuthority('书籍查看')")
    @RequestMapping("/findBookById")
    public ResponseResult findBookById(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        Integer id = jsonObject.getInteger("id");

        Book theBook = bookService.findBookById(id);
        Map<String, Book> map = new HashMap<>();
        map.put("theBook",theBook);
        return new ResponseResult(200,"查找成功",map);
    }

    @PreAuthorize("hasAuthority('书籍查看')")
    @RequestMapping("/findBookByAuthor")
    public ResponseResult findBookByAuthor(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        String author = jsonObject.getString("author");

        List<Book> books = bookService.findBookByAuthor(author);
        Map<String,List<Book>> map = new HashMap<>();
        map.put("books",books);
        return new ResponseResult(200,"查找成功",map);
    }

    @PreAuthorize("hasAuthority('书籍查看')")
    @RequestMapping("/findBookByName")
    public ResponseResult findBookByName(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        String bookName = jsonObject.getString("bookName");

        Book theBook = bookService.findBookByName(bookName);
        Map<String, Book> map = new HashMap<>();
        map.put("theBook",theBook);
        return new ResponseResult(200,"查找成功",map);
    }

    @PreAuthorize("hasAuthority('书籍查看')")
    @RequestMapping("/findBookByType")
    public ResponseResult fondBookByType(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        String bookType = jsonObject.getString("bookType");

        List<Book> books = bookService.findBookByType(bookType);
        Map<String, List<Book>> map = new HashMap<>();
        map.put("books",books);
        return new ResponseResult(200,"查找成功",map);
    }

    @PreAuthorize("hasAuthority('书籍查看')")
    @RequestMapping("/getBookmark")
    public ResponseResult getBookmark(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        int bookId = jsonObject.getInteger("bookId");
        Long userId = loginService.getLoginUser().getId();
        Map<String,Integer> map = new HashMap<>();
        int bookmark = bookService.getBookmark(userId,bookId);
        map.put("bookmark",bookmark);
        return new ResponseResult(200,"查找成功",map);
    }

    @PreAuthorize("hasAuthority('书籍查看')")
    @RequestMapping("/getChapter")
    public ResponseResult getChapter(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        int numbering = jsonObject.getInteger("numbering");
        int bookId = jsonObject.getInteger("bookId");
        Chapter chapter = chapterService.getChapterByBookIdAndNumbering(bookId,numbering);
        Map<String,Chapter> map = new HashMap<>();
        map.put("chapter",chapter);
        return new ResponseResult(200,"查找成功",map);
    }

    @PreAuthorize("hasAuthority('书籍查看')")
    @RequestMapping("/getChapterByBookId")
    public ResponseResult getAllChapter(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        int bookId = jsonObject.getInteger("bookId");
        List<Chapter> chapters = chapterService.getChapterByBookId(bookId);
        Map<String,List<Chapter>> map = new HashMap<>();
        map.put("chapters",chapters);
        return new ResponseResult(200,"查找成功",map);
    }



    @PreAuthorize("hasAuthority('书籍编辑')")
    @RequestMapping("/addBook")
    public ResponseResult addBook(@RequestBody Book book){
        if (bookService.addBook(book)==1){
            return new ResponseResult(200,"上传成功");
        }else {
            return new ResponseResult(250,"上传失败");
        }
    }

    @PreAuthorize("hasAuthority('书籍编辑')")
    @RequestMapping("/updateBook")
    public ResponseResult updateBook(@RequestBody Book book){
        if (bookService.update(book)==1){
            return new ResponseResult(200,"修改成功");
        }else {
            return new ResponseResult(250,"修改失败");
        }
    }

    @PreAuthorize("hasAuthority('书籍编辑')")
    @RequestMapping("/addCover")
    public ResponseResult addCover(@RequestParam("bookId")Integer bookId, MultipartFile file) throws IOException {
        Book book = bookService.findBookById(bookId);
        String flag = fileService.upload(file);
        book.setCover(flag);
        if (bookService.update(book)==1){
            Map<String,String> map = new HashMap<>();
            map.put("CoverUrl",flag);
            return new ResponseResult(200,"封面上传成功",map);
        }else {
            return new ResponseResult(250,"封面上传失败");
        }
    }

    @PreAuthorize("hasAuthority('书籍编辑')")
    @RequestMapping("/addChapterByFile")
    public ResponseResult addChapterByFile(@RequestParam("bookId")Integer bookId, MultipartFile file) throws IOException {
        String filePath = fileService.upload(file);
        String originalFilename = file.getOriginalFilename();
        Chapter chapter = new Chapter();
        chapter.setBookId(bookId);
        chapter.setChapterName(originalFilename);
        chapter.setFilePath(filePath);
        int i = bookService.addChapter(chapter);
        if (i==1){
            return new ResponseResult(200,"章节上传成功");
        }else {
            return new ResponseResult(250,"章节上传失败");
        }
    }

    @PreAuthorize("hasAuthority('书籍查看')")
    @RequestMapping("/getContent")
    public ResponseResult getContent(@RequestBody String json) throws IOException {
        JSONObject jsonObject = JSONObject.parseObject(json);
        int numbering = jsonObject.getInteger("numbering");
        int bookId = jsonObject.getInteger("bookId");
        Chapter theChapter = chapterService.getChapterByBookIdAndNumbering(bookId,numbering);
        if (theChapter == null){
            return new ResponseResult(250,"该章节不存在");
        }
        int chapterId = theChapter.getId();
        Chapter chapter = bookService.getChapterById(chapterId);
        String basePath = System.getProperty("user.dir") + "/files/";  // 定于文件上传的根路径
        List<String> fileNames = FileUtil.listFileNames(basePath);  // 获取所有的文件名称
        String[] sourceArray = chapter.getFilePath().split("/");
        String flag = sourceArray[sourceArray.length-1];
        String fileName = fileNames.stream().filter(name -> name.contains(flag)).findAny().orElse("");
        String pathname = basePath+fileName; // 绝对路径或相对路径都可以，这里是绝对路径，写入文件时演示相对路径
        File file = new File(pathname); // 要读取以上路径的input。txt文件
        InputStreamReader reader = new InputStreamReader(new FileInputStream(file),"UTF-8"); // 建立一个输入流对象reader
        BufferedReader br = new BufferedReader(reader); // 建立一个对象，它把文件内容转成计算机能读懂的语言
        List<String> content = new ArrayList<>();
        String line = "";
        line = br.readLine();
        content.add(line);
        while (line != null) {
            line = br.readLine(); // 一次读入一行数据
            content.add(line);
        }
        Map<String,List<String>> map = new HashMap<>();
        map.put("content",content);
        return new ResponseResult(200,"查找成功",map);
    }

    @PreAuthorize("hasAuthority('书籍编辑')")
    @RequestMapping("/setContent")
    public ResponseResult setContent(@RequestBody String json) throws IOException {
        System.out.println(json);
        System.out.println(json);
        System.out.println(json);
        System.out.println(json);
        JSONObject jsonObject = JSONObject.parseObject(json);
        int bookId = jsonObject.getInteger("bookId");
        String chapterName = jsonObject.getString("chapterName");
        String content = jsonObject.getString("content");
        String flag = IdUtil.fastSimpleUUID();
        String rootFilePath = System.getProperty("user.dir") + "/files/" + flag + "_" + chapterName;
        File writename = new File(rootFilePath); // 相对路径，如果没有则要建立一个新的output。txt文件
        writename.createNewFile(); // 创建新文件
        BufferedWriter out = new BufferedWriter(new FileWriter(writename));
        out.write(content); // \r\n即为换行
        out.flush(); // 把缓存区内容压入文件
        out.close(); // 最后记得关闭文件
        String filePath = "http://" + ip + ":" + port + "/files/" + flag;
        Chapter chapter = new Chapter();
        chapter.setBookId(bookId);
        chapter.setChapterName(chapterName);
        chapter.setFilePath(filePath);
        int i = bookService.addChapter(chapter);
        if (i==1){
            return new ResponseResult(200,"添加成功");
        }else {
            return new ResponseResult(250,"添加失败");
        }
    }

}


package com.work.bootserver.controller;


import com.alibaba.fastjson.JSONObject;
import com.work.bootserver.entity.Reply;
import com.work.bootserver.entity.ResponseResult;
import com.work.bootserver.mapper.ReplyMapper;
import com.work.bootserver.service.IReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
@RestController
@RequestMapping("/reply")
public class ReplyController {
    @Autowired
    private IReplyService replyService;
    @PreAuthorize("hasAuthority('添加评论')")
    @RequestMapping("/addReply")
    public ResponseResult addReply(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        int commentId = jsonObject.getInteger("commentId");
        int userId = jsonObject.getInteger("userId");
        String message = jsonObject.getString("message");
        Reply reply = new Reply();
        reply.setCommentId(commentId);
        reply.setMessage(message);
        reply.setUserId(userId);
        reply.setCreateTime(LocalDateTime.now());
        if (replyService.addReply(reply)==1){
            return new ResponseResult(200,"回复成功");
        }else {
            return new ResponseResult(250,"回复失败");
        }
    }
    @PreAuthorize("hasAuthority('查看评论')")
    @RequestMapping("/findReplyByCommentId")
    public ResponseResult findReplyByCommentId(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        int commentId = jsonObject.getInteger("commentId");
        List<Reply> replies = replyService.findReplyByCommentId(commentId);
        Map<String,List<Reply>> map = new HashMap<>();
        map.put("replies",replies);
        return new ResponseResult(200,"查找成功",map);
    }
}


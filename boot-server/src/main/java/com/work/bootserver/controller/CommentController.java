package com.work.bootserver.controller;


import com.alibaba.fastjson.JSONObject;
import com.work.bootserver.entity.Chapter;
import com.work.bootserver.entity.Comment;
import com.work.bootserver.entity.Paragraph;
import com.work.bootserver.entity.ResponseResult;
import com.work.bootserver.service.ICommentService;
import com.work.bootserver.service.IParagraphService;
import com.work.bootserver.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
@RestController
@RequestMapping("/comment")
public class CommentController {
    @Autowired
    private LoginService loginService;
    @Autowired
    private ICommentService commentService;
    @Autowired
    private IParagraphService paragraphService;
    @PreAuthorize("hasAuthority('添加评论')")
    @RequestMapping("/addBookComment")
    public ResponseResult addBookComment(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        Comment comment = new Comment();
        int bookId = jsonObject.getInteger("bookId");
        comment.setBookId(bookId);
        Long userId = loginService.getLoginUser().getId();
        comment.setUserId(userId);
        String message = jsonObject.getString("message");
        comment.setMessage(message);
        comment.setCreateTime(LocalDateTime.now());
        if (commentService.addComment(comment)==1){
            return new ResponseResult(200,"添加成功");
        }else {
            return new ResponseResult(250,"添加失败");
        }
    }
    @PreAuthorize("hasAuthority('添加评论')")
    @RequestMapping("/addParagraphComment")
    public ResponseResult addParagraphComment(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        Long userId = loginService.getLoginUser().getId();
        String message = jsonObject.getString("message");
        int bookId = jsonObject.getInteger("bookId");
        int numbering = jsonObject.getInteger("numbering");
        int chapterId = jsonObject.getInteger("chapterId");
        Comment comment = new Comment();
        comment.setUserId(userId);
        comment.setBookId(bookId);
        comment.setMessage(message);
        comment.setCreateTime(LocalDateTime.now());
        int i = commentService.addComment(comment);
        int commeentId = commentService.getLastId();
        Paragraph paragraph = new Paragraph();
        paragraph.setCommentId(commeentId);
        paragraph.setChapterId(chapterId);
        paragraph.setNumbering(numbering);
        int j = commentService.addParagraph(paragraph);
        if (i==1&&j==1){
            return new ResponseResult(200,"评论成功");
        }else {
            return new ResponseResult(250,"评论失败");
        }
    }

    @PreAuthorize("hasAuthority('查看评论')")
    @RequestMapping("/getParagraphComment")
    public ResponseResult getParagraphComment(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        int numbering = jsonObject.getInteger("numbering");
        int chapterId = jsonObject.getInteger("chapterId");
        int bookId = jsonObject.getInteger("bookId");
        List<Comment> paragraphComment = commentService.getParagraphComment(chapterId, numbering,bookId);
        Map<String,List<Comment>> map = new HashMap<>();
        map.put("paragraphComment",paragraphComment);
        return new ResponseResult(200,"查找成功",map);

//        List<Integer> commentIds = commentService.getParagraphComment(numbering,chapterId);
    }

    @PreAuthorize("hasAuthority('查看评论')")
    @RequestMapping("/getParagraphNumber")
    public ResponseResult getParagraphNumber(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        int chapterId = jsonObject.getInteger("chapterId");
        List<Paragraph> paragraphs = paragraphService.findParagraphByChapterId(chapterId);
        Map<String,Integer> map = new HashMap<>();
        for (Paragraph paragraph :paragraphs){
            int number = paragraphService.findparagraphByChapterIdAndNumbering(chapterId,paragraph.getNumbering()).size();
            map.put(paragraph.getNumbering().toString(),number);
        }
        return new ResponseResult(200,"查找成功",map);
    }
//    @RequestMapping("/getLastId")
//    public int getLastId(){
//        return commentService.getLastId();
//    }
}


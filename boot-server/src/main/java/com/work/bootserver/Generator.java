//package com.work.bootserver;
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.generator.AutoGenerator;
//import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
//import com.baomidou.mybatisplus.generator.config.GlobalConfig;
//import com.baomidou.mybatisplus.generator.config.PackageConfig;
//import com.baomidou.mybatisplus.generator.config.StrategyConfig;
//
//public class Generator {
//    public static void main(String[] args) {
//        AutoGenerator autoGenerator = new AutoGenerator();
//
//        DataSourceConfig dataSourceConfig = new DataSourceConfig();
//        dataSourceConfig.setUrl("jdbc:mysql://localhost:3306/test_comment?useUnicode=true&characterEncoding=utf8");
//        dataSourceConfig.setDriverName("com.mysql.cj.jdbc.Driver");
//        dataSourceConfig.setUsername("root");
//        dataSourceConfig.setPassword("root");
//        autoGenerator.setDataSource(dataSourceConfig);
//        //全局配置
//        GlobalConfig globalConfig = new GlobalConfig();
//        globalConfig.setOutputDir(System.getProperty("user.dir") + "\\src\\main\\java");
//        globalConfig.setOpen(false);
//        globalConfig.setAuthor("suLang");
//        globalConfig.setFileOverride(true);//覆盖
//        globalConfig.setIdType(IdType.ASSIGN_ID);
//        autoGenerator.setGlobalConfig(globalConfig);
//        //设置包相关配置
//        PackageConfig packageConfig = new PackageConfig();
//        packageConfig.setParent("com.work.bootserver");
////        packageConfig.setEntity()
////        packageConfig.setMapper()
////        packageConfig.setController()
//        autoGenerator.setPackageInfo(packageConfig);
//
//        //策略设置
//        StrategyConfig strategyConfig = new StrategyConfig();
//        strategyConfig.setTablePrefix("sys_");
//        strategyConfig.setEntityLombokModel(true);//使用Lombok
//        strategyConfig.setRestControllerStyle(true);//变为restController
////        strategyConfig.setLogicDeleteFieldName("deleted");//设置逻辑删除字段名
////        strategyConfig.setVersionFieldName("version");//设置乐观锁字段名
//        strategyConfig.setInclude("chapter");//设置当前参与生成的表名
//        autoGenerator.setStrategy(strategyConfig);
//        autoGenerator.execute();
//    }
//}
//

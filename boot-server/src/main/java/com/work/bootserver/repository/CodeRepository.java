package com.work.bootserver.repository;

import com.work.bootserver.entity.Code;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Repository
public class CodeRepository {
    @SuppressWarnings("unused")
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @SuppressWarnings("unused")
    @Resource
    private RedisTemplate<Object,Object> redisTemplate;

//    @Resource(name = "stringRedisTemplate")
//    ValueOperations<String,String> valueOpsStr;

    @Resource(name = "redisTemplate")
    ValueOperations<Object,Object> valueOpsObject;

//    public void saveCode(String key,String value){
//        TimeUnit timeUnit = TimeUnit.SECONDS;
//        valueOpsStr.set(key,value,100,timeUnit);
//    }

    public void saveCode(String key, Code value){
        TimeUnit timeUnit = TimeUnit.SECONDS;
        valueOpsObject.set(key,value,300,timeUnit);
    }

//    public String getCode(String key){
//        String code = valueOpsStr.get(key);
//        if (code == null){
//            return "delayed";
//        }else {
//            return code;
//        }
//    }

    public Object getCode(Object key){
        Object code = valueOpsObject.get(key);
        if (code == null){
            return "delayed";
        }else {
            return code;
        }
    }
}

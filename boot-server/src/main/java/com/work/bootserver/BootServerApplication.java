package com.work.bootserver;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication
@MapperScan(basePackages = "com.work.bootserver.mapper")
public class BootServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootServerApplication.class, args);
	}

}

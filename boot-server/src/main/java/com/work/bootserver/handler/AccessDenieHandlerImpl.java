package com.work.bootserver.handler;

import cn.hutool.http.HttpStatus;
import com.alibaba.fastjson.JSON;
import com.work.bootserver.entity.ResponseResult;
import com.work.bootserver.utils.WebUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@Component
public class AccessDenieHandlerImpl implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        ResponseResult result = new ResponseResult(HttpStatus.HTTP_FORBIDDEN,"权限不足，无法访问");
        String s = JSON.toJSONString(result);
        WebUtils.renderString(response,s);
    }
}

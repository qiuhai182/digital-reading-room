package com.work.bootserver.entity;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Paragraph implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 章节id
     */
    private Integer chapterId;

    /**
     * 句子评论id
     */
    private Integer commentId;

    /**
     * 这个章节的第几句
     */
    private Integer numbering;


}

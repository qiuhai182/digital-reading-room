package com.work.bootserver.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Code implements Serializable {
    private String codeNumber;
    private String codeType;

//    public Code(String codeNumber, String codeType) {
//        this.codeNumber = codeNumber;
//        this.codeType = codeType;
//    }
}

package com.work.bootserver.mapper;

import com.work.bootserver.entity.User_role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author suLang
 * @since 2022-06-07
 */
@Mapper
public interface User_roleMapper extends BaseMapper<User_role> {

}

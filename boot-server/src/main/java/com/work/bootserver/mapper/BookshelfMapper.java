package com.work.bootserver.mapper;

import com.work.bootserver.entity.Bookshelf;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author suLang
 * @since 2022-06-06
 */
@Mapper
public interface BookshelfMapper extends BaseMapper<Bookshelf> {

    public Bookshelf selectByUserIdAndBookId(Long userId, int bookId);
}

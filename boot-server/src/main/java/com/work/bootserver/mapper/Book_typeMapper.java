package com.work.bootserver.mapper;

import com.work.bootserver.entity.Book_type;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author suLang
 * @since 2022-06-06
 */
public interface Book_typeMapper extends BaseMapper<Book_type> {

}

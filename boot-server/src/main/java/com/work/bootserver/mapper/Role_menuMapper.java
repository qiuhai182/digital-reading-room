package com.work.bootserver.mapper;

import com.work.bootserver.entity.Role_menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author suLang
 * @since 2022-06-07
 */
public interface Role_menuMapper extends BaseMapper<Role_menu> {

}

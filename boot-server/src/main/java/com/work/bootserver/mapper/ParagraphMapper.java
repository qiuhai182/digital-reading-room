package com.work.bootserver.mapper;

import com.work.bootserver.entity.Paragraph;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
@Mapper
public interface ParagraphMapper extends BaseMapper<Paragraph> {
    public List<Paragraph> selectByChapterId(int chpaterId);

    List<Paragraph> selectByChapterIdAndNumbering(int chapterId, Integer numbering);
//    public List<Integer> selectCommentId(int chapterId, int numbering);
}

package com.work.bootserver.mapper;

import com.work.bootserver.entity.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.work.bootserver.entity.Paragraph;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
@Mapper
public interface CommentMapper extends BaseMapper<Comment> {

    public int getLastId();

    public List<Comment> selectParagraphComment(int chapterId, int numbering, int bookId);
}

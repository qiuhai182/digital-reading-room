package com.work.bootserver.mapper;

import com.work.bootserver.entity.Book;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author suLang
 * @since 2022-06-06
 */
@Mapper
public interface BookMapper extends BaseMapper<Book> {
    public List<Book> selectByAuthor(String author);

    public Book selectByName(String bookName);

    public List<Book> selectByType(String bookType);

    public int getBookmark(Long userId, int bookId);
}

package com.work.bootserver.mapper;

import com.work.bootserver.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author suLang
 * @since 2022-06-07
 */
public interface RoleMapper extends BaseMapper<Role> {

}

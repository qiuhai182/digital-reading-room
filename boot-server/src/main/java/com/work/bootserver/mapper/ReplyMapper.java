package com.work.bootserver.mapper;

import com.work.bootserver.entity.Reply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
@Mapper
public interface ReplyMapper extends BaseMapper<Reply> {

    public List<Reply> selectByCommentId(int commentId);
}

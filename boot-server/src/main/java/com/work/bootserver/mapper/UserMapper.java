package com.work.bootserver.mapper;

import com.work.bootserver.entity.Book;
import com.work.bootserver.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author suLang
 * @since 2022-06-07
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    public User selectByemail(String email);
    public User selectByName(String userName);
    public List<Book> selectMyFavorite (Long id);
    public User selectById(Long id);
}

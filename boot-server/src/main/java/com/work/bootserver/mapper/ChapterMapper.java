package com.work.bootserver.mapper;

import com.work.bootserver.entity.Chapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author suLang
 * @since 2022-06-10
 */
@Mapper
public interface ChapterMapper extends BaseMapper<Chapter> {

    public Chapter selectByBookIdAndNumbering(int bookId, int numbering);

    public List<Chapter> selectByBookId(int bookId);
}

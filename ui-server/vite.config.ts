import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
// import path from 'path'
const path = require('path')
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    // 配置路径别名
    alias: {
      '@': path.resolve(__dirname, './src'),
    },
  },
  server:{
    open:true,//自动打开浏览器
    port:80,
    proxy:{
      '/api':{
        target:'http://localhost:8888',
        changeOrigin:true,
        rewrite:(path)=>path.replace(/^\/api/,'')
      }
    }
  }
})
import * as path from '@/store/index'

export function setToSession(){
    const bookStore = path.bookStore()
    const userStore = path.userStore()
    userStore.toSession()
    bookStore.toSession()
}
export function getToSession(){
    const bookStore = path.bookStore()
    const userStore = path.userStore()
    userStore.fromSession()
    bookStore.fromSession()
}
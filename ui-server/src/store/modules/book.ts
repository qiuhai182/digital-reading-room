import { store } from '@/store'
import { defineStore } from 'pinia'
// import { TOKEN_NAME } from '@/config/global'
import { sessionStorage } from '@/utils/storage'
import request from "@/utils/request";
import {MessagePlugin} from "tdesign-vue-next";


export const bookStore = defineStore('book', {
    state: () => ({
        // token: localStorage.getItem(TOKEN_NAME),
        bookInfo: {
            bookId:'',
            bookName:'',
            author:'',
            chapter:''
        },
    }),
    getters: {
        getBookInfo: (state) => {
            return state.bookInfo
        },
    },
    actions: {
        toSession() {
            sessionStorage.set('bookInfo', this.bookInfo)
        },
        fromSession() {
            this.bookInfo = sessionStorage.get('bookInfo')
            sessionStorage.remove('bookInfo')
        },
        setBookInfo(info) {
            this.bookInfo = info
        },
    },
})

export function getBookInfo() {
    return bookStore(store)
}

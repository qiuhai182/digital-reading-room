import { store } from '@/store'
import { defineStore } from 'pinia'
// import { TOKEN_NAME } from '@/config/global'
import { sessionStorage } from '@/utils/storage'
import request from "@/utils/request";
import {MessagePlugin} from "tdesign-vue-next";


export const userStore = defineStore('user', {
    state: () => ({
        // token: localStorage.getItem(TOKEN_NAME),
        userInfo: {
            userId:'',
            userName:'',
            avatar:'',
            email:''
        },
    }),
    getters: {
        getUserInfo: (state) => {
            return state.userInfo
        },
    },
    actions: {
        toSession() {
            sessionStorage.set('userInfo', this.userInfo)
        },
        fromSession() {
            this.userInfo = sessionStorage.get('userInfo')
            sessionStorage.remove('userInfo')
        },
        setUserInfo(info) {
            this.userInfo = info
        },
    },
})

export function getUserInfo() {
    return userStore(store)
}

// @ts-ignore
import { createPinia } from 'pinia'

const store = createPinia()

export { store }

export * from './modules/book'
export * from './modules/user'
// export * from './modules/permission'



export default store

import authorRouter from '@/router/modules/authorRouter'
import cityRouter from "@/router/modules/cityRouter";

export default [
    {
        path: 'bookshelf',
        name: 'bookshelf',
        component: () => import('@/pages/bookshelf/index.vue'),
        props: false //不传参
    },
    {
        path: 'bookCity',
        name: 'bookCity',
        component: () => import('@/pages/bookCity/index.vue'),
        props: false, //不传参
        children: [...cityRouter],
    },
    {
        path: 'chapter',
        name: 'chapter',
        component: () => import('@/pages/chapter/index.vue'),
        // props: false, //不传参
    },
    {
        path: 'personal',
        name: 'personal',
        component: () => import('@/pages/personal/index.vue'),
        props: false //不传参
    },
    {
        path: 'authorManage',
        name: 'authorManage',
        component: () => import('@/pages/authorManage/index.vue'),
        props: false, //不传参
        children: [...authorRouter],
    },
    {
        path: 'userManager',
        name: 'userManager',
        component: () => import('@/pages/userManager/index.vue'),
    }
]


export default [
    {
        path: 'homeCity',
        name: 'homeCity',
        component: () => import('@/pages/bookCity/components/HomeCity.vue'),
        props: false //不传参
    },
    {
        path: 'bookList',
        name: 'bookList',
        component: () => import('@/pages/bookCity/components/BookList.vue'),
        props: false //不传参
    },
    {
        path: 'ranking',
        name: 'ranking',
        component: () => import('@/pages/bookCity/components/Ranking.vue'),
        props: false //不传参
    },
    {
        path: 'allBook',
        name: 'allBook',
        component: () => import('@/pages/bookCity/components/AllBook.vue'),
        props: false //不传参
    },
]

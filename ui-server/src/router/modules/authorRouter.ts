export default [
    {
        path: 'authorNotice',
        name: 'authorNotice',
        component: () => import('@/pages/authorNotice/index.vue'),
        props: false //不传参
    },
    {
        path: 'longWorkManage',
        name: 'longWorkManage',
        component: () => import('@/pages/longWorkManage/index.vue'),
        props: false //不传参
    },
    {
        path: 'mediumShortWorkManage',
        name: 'mediumShortWorkManage',
        component: () => import('@/pages/mediumShortWorkManage/index.vue'),
        props: false //不传参
    },
    {
        path: 'bookData',
        name: 'bookData',
        component: () => import('@/pages/bookData/index.vue'),
        props: false //不传参
    },
    {
        path: 'remunerationIncome',
        name: 'remunerationIncome',
        component: () => import('@/pages/remunerationIncome/index.vue'),
        props: false //不传参
    },
    {
        path: 'welfareIncome',
        name: 'welfareIncome',
        component: () => import('@/pages/welfareIncome/index.vue'),
        props: false //不传参
    },
    {
        path: 'copyrightIncome',
        name: 'copyrightIncome',
        component: () => import('@/pages/copyrightIncome/index.vue'),
        props: false //不传参
    },
    {
        path: 'authorMessage',
        name: 'authorMessage',
        component: () => import('@/pages/authorMessage/index.vue'),
        props: false //不传参
    },
    {
        path: 'authorInformation',
        name: 'authorInformation',
        component: () => import('@/pages/authorInformation/index.vue'),
        props: false //不传参
    },
    {
        path: 'authorComment',
        name: 'authorComment',
        component: () => import('@/pages/authorComment/index.vue'),
        props: false //不传参
    },
    {
        path: 'newBook',
        name: 'newBook',
        component: () => import('@/pages/newBook/index.vue'),
        props: false //不传参
    },
    {
        path: 'newChapter',
        name: 'newChapter',
        component: () => import('@/pages/newChapter/index.vue'),
        props: false //不传参
    }
]

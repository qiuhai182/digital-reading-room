import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import components from '@/router/modules/components'

const routes: Array<RouteRecordRaw> = [
    // 根路由提供注册、登录等基础服务
    {
        path: '/',
        name:'login',
        component: () => import('@/pages/login/index.vue')
    },
    {
        path:'/register/:param',
        name:'register',
        component:() => import('@/pages/register/index.vue')
    },
    {
        // 子路由负责提供功能性服务
        path:'/digitalReadingRoom',
        name:'digitalReadingRoom',
        component: () => import('@/layouts/index.vue'),
        children: [...components],
    }
]

const router = createRouter({
    // createWebHashHistory hash路由
    // createWebHistory history路由
    // createMemoryHistory 带缓存history路由
    history: createWebHistory(),
    routes
})

export default router
